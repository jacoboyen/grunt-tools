Central College Grunt Tools
===================

 A set of tools to streamline website optimization including:
* JavaScript concatenation and compression
* Image compression and optimization

Requirements
------------------
* Git - To clone this repo and periodically update
* node.js - http://nodejs.org

Setup
------------------

Follow these instructions to get started:

* Install node.js on your machine
* Clone the repo to a local directory (git clone https://bitbucket.org/jacoboyen/grunt-tools.git [local directory name])
* Create these sub-directories: 
	* dev/css
	* dev/images
	* dev/javascripts
	* provider/css
	* provider/images
	* provider/javascripts
* Open the directory at the command line and run "npm install"

After setup
------------------
Edit gruntfile.js so that the copy function copies files to the location of your choice.
After install is completed run "grunt" at the command line.

Additional notes
------------------
Any specific grunt task can be run individually. Example: "grunt concat". Available options are:

* grunt concat (Concatenate javascripts)
* grunt uglify (Minimize javascripts)
* grunt imagemin (Optimize images)

It may be helpful to periodically do a git fetch to get the latest grunt features.