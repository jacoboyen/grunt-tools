 module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		
		//Combine JS files
		concat:{
			dist:{
				src:[
					'central-homepage/javascripts/main/scripts-to-use/*js',
				],
				dest: 'central-homepage/javascripts/main/homepage.js',
			}
		},
		
		// Minify all JS files
		uglify:{
			build:{
				src: 'central-homepage/javascripts/main/homepage.js',
				dest: 'central-homepage/javascripts/main/homepage.min.js'
			}
		},
		
		//Optimize images
		//You may need to run "npm install jpegtran-bin@0.2.0" so that the minimize images feature works properly.
		imagemin:{
			dynamic:{
				files:[{
					expand:true,
					cwd: 'images/',
					src: ['**/*.{png,jpg,gif}'],
					dest: 'images'
				}]
			}
		},
		
		//Copy files to directory of your choice (points to your H: drive currently)
		copy: {
		  index: {
			src: 'central-homepage/index.cfm',
			dest: 'index.cfm',
		  },
		  css: {
			src: 'central-homepage/css/main/homepage.css',
			dest: 'css/main/homepage.css' 
		  },
		  javascripts: {
			src: 'central-homepage/javascripts/main/homepage.min.js',
			dest: 'javascripts/main/homepage.min.js',
		  }
		},	
		
		cssmin:{
			minify:{
				expand: true,
				cwd: 'css/main/',
				src: ['*.css', '!*.min.css'],
				dest: 'css/main/',
				ext: '.min.css'
			}
		},
		
		//Watch folders
		watch:{
			options: {
				livereload: true,
			},
			css:{
				files:'central-homepage/javascripts/main/homepage.css',
				tasks:['cssmin'],
				options:{
					interrupt:true
				}
			},
			images:{
				files:'images/*.{png,jpg,gif}',
				tasks:['imagemin'],
				options:{
					interrupt:true
				}
			},
			javascripts:{
				files:'central-homepage/javascripts/main/scripts-to-use/*.js',
				tasks:['concat','uglify'],
				options:{
					interrupt:true
				}
			},
			copy:{
				files:'central-homepage/**',
				tasks:['copy'],
				options:{
					interrupt:true
				}	
			}
			
		}
	});
	
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	
	grunt.registerTask('default', ['watch']);
};
	